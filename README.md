# Takehome App

## Overview

This is a small application that implements the following user stories:

1. As a user I want to submit a sentence and see the counts of unique consecutive pairs of words.
2. As a user I want to see my past submissions of sentences and their counts.
3. As a user I want to click a past submission so I can alter it and re-submit.
4. (Special!) As a user I want to see a visual representation of the pair counts.

![A quick look](screencap.png)

## Tech Stack

- Python 3.8 (nothing 3.8 specific though)

- Flask for server framework (backend code in `app`)

- Pytest for testing backend code

- Sqlite integrated with Flask-SQLAlchemy for database and schema

- React for frontend framework (create-react-app for bootstrapping with code in `client`)

- NPM version 12.18.4

- Docker for deployment (using v20.10.3)

## Running quickly

### Prerequisites
Have make and docker installed.

Make sure ports `:80` and `:5000` are free.

### To Run

    make docker-pull-run

And open up your favorite web browser to http://127.0.0.1

Docker image hosted [here](https://hub.docker.com/r/scottlindeman/takehome).

## Development

### Backend

I have checked in the python virtualenv for convenience, but traditionally setup the virtualenv.

    python3 -m venv aclima_app
    source aclima_app/bin/activate
    pip install -r requirements.txt

To run the backend:

    python app/main.py

### Frontend

I have checked in the entire node_modules for convenience, but traditionally setup the environment.

If you don't have it, install `nvm` from [here](https://github.com/nvm-sh/nvm#install--update-script).

Then run:

    cd client
    npm install
    npm start # (fingers crossed)

### Docker

You can run both in docker locally using the `make docker-*` commands as well.

    make docker-build
    make docker-run

This runs both the frontend and the backend together, exposed on `:3000` and `:5000` respectively.

NOTE: This does require that `client/node_modules` has been built locally. An improvement for later.

### Testing

To run tests:

    export PYTHONPATH=$(pwd)
    make unit-test

## Improvements still to make

1. Better API request payload validation

2. Pagination in the list route, right now we're querying for and sending back everything

3. Integration tests for the backend application, testing both flask and the db interactions

   a. Flask-SQLAlchemy forced me to do some gross things I normally don't like doing, such as importing inside functions to get around circular imports.

4. Unit tests for the frontend

5. Setting up a production docker deployment system, currently still runs using dev servers

6. Logging for both frontend and backend

7. Better error handling for both frontend and backend

8. Probably quite a few more things, but this is where I am comfortable submitting.
