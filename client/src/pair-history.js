import React from 'react';

class PairItem extends React.Component {
  constructor(props) {
    super(props);

    this.onClick = this.onClick.bind(this);

  }

  onClick(event) {
    this.props.valueCallback(this.props.pairItem.entry);
    this.props.pairCallback(this.props.pairItem);
    event.preventDefault();
  }

  render() {
    return (
        <div className="pure-u-1 pair-item" onClick={this.onClick}>
        <h4 className="pair-item-entry">{this.props.pairItem.entry}</h4>
        <table class="pure-table">
        <thead>
        <tr>
        <th>Pair</th>
        <th>Count</th>
        </tr>
        </thead>
        <tbody>
        {this.props.pairItem.results.map(
          (result) => (
              <tr>
                <td>{result.pair}</td>
                <td>{result.count}</td>
              </tr>
          )
        )}
      </tbody>
        </table>
      </div>
    );
  }

}

class PairHistory extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="pure-u-2-5 pair-history">
      {this.props.pairItems.map(
        (pairItem) => (
            <PairItem  pairItem={pairItem} valueCallback={this.props.valueCallback} pairCallback={this.props.pairCallback} />
        )
      )}
      </div>
    );
  }

}

export default PairHistory;
