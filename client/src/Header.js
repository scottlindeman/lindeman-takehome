function Header() {
  return (
      <div class="header">
        <div class="home-menu pure-menu pure-menu-horizontal pure-menu-fixed">
          <span class="pure-menu-heading">Aclima</span>
        </div>
      </div>
  );
}

export default Header;
