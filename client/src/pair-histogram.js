import React from 'react';
import * as d3 from 'd3';

class PairHistogram extends React.Component {
  constructor(props) {
    super(props);
    this.renderHistogram = this.renderHistogram.bind(this);
  }

  componentDidMount() {
    this.renderHistogram();
  }

  componentDidUpdate() {
    this.renderHistogram();
  }

  renderHistogram() {
    if (!this.props.selectedPair) {
      return;
    }
    d3.select('.histogram-render svg').remove();

    const margin = 30;
    const width = 450 - (2 * margin);
    const height = 200 - (4 * margin);

    const svg = d3.select('.histogram-render')
          .append('svg')
          .attr('width', width + (2 * margin))
          .attr('height', height + (4 * margin))
          .append('g')
          .attr('transform', `translate(${margin}, ${margin})`);

    const pairs = this.props.selectedPair.results;

    const xAxis = d3.scalePoint()
          .domain(pairs.map((d) => d.pair).concat(['']))
          .range([0, width]);

    svg.append('g')
      .attr('transform', `translate(0, ${height})`)
      .call(
        d3.axisBottom(xAxis)
          .ticks(pairs.length + 1)
      )
      .selectAll('Text')
      .attr('y', 0)
      .attr('x', 9)
      .attr('dy', '.35em')
      .attr('transform', 'rotate(45)')
      .style('text-anchor', 'start');

    const maxCount = d3.max(pairs, (d) => d.count);

    const yAxis = d3.scaleLinear()
          .domain([0, maxCount])
          .range([height, 0]);

    svg.append('g')
      .call(
        d3.axisLeft(yAxis)
          .ticks(maxCount)
      );

    svg.selectAll('rect')
      .data(pairs)
      .enter()
      .append('rect')
      .attr('x', 1)
      .attr('transform', (d) => `translate(${xAxis(d.pair)}, ${yAxis(d.count)})`)
      .attr('width', width/(pairs.length))
      .attr('height', (d) => height - yAxis(d.count))
      .style('fill', '#4198AA');
  }

  render() {
    return (
        <div className="pure-u-1-2 pair-histogram">
        <div className="histogram-render" />
        </div>
    );
  }

}

export default PairHistogram;
