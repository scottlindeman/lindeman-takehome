import axios from 'axios';

function PairService() {

  const BASE_URL = 'http://localhost:5000';
  const PAIR_URL = `${BASE_URL}/pair`;

  return {
    submit: async (userStr) => {
      const result = await axios.post(PAIR_URL, {userStr: userStr});
      console.log(result);
    },

    list: async () => {
      const result = await axios.get(PAIR_URL);
      const items = result.data;
      items.reverse();
      return items;
    }

  };
}

export default PairService;
