import React from 'react';
import ReactDOM from 'react-dom';
import AclimaApp from './App';
import Header from './Header.js';

ReactDOM.render(
  <React.StrictMode>
    <Header />
    <AclimaApp />
  </React.StrictMode>,
  document.getElementById('root')
);
