import React from 'react';

import {DEFAULT_VALUE, PairForm} from './pair-form.js';
import PairHistory from './pair-history.js';
import PairHistogram from './pair-histogram.js';
import PairService from './pair-service.js';

class AclimaApp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      inputValue: DEFAULT_VALUE,
      selectedPair: null,
      pairItems: []
    };

    this.pairService = PairService();

    this.setInputValue = this.setInputValue.bind(this);
    this.setSelectedPair = this.setSelectedPair.bind(this);
    this.refreshPairItems = this.refreshPairItems.bind(this);
  }

  async componentDidMount() {
    await this.refreshPairItems();
  }

  async refreshPairItems() {
    const pairItems = await this.pairService.list();
    this.setState({pairItems: pairItems});
  }

  setInputValue(newValue) {
    this.setState({inputValue: newValue});
  }

  setSelectedPair(newSelection) {
    this.setState({selectedPair: newSelection});
  }

  render() {
    return (
        <div className="pure-g content-container">
          <h1 className="pure-u-1">Please, type a sentence and press "Enter"</h1>
          <PairForm
            valueCallback={this.setInputValue}
            refreshCallback={this.refreshPairItems}
            value={this.state.inputValue}
          />
          <PairHistory
            valueCallback={this.setInputValue}
            pairCallback={this.setSelectedPair}
            pairItems={this.state.pairItems}
        />
        <PairHistogram selectedPair={this.state.selectedPair} />
        </div>
    );
  }
}

export default AclimaApp;
