import React from 'react';
import PairService from './pair-service.js';

const DEFAULT_VALUE = '';

class PairForm extends React.Component {
  constructor(props) {
    super(props);
    this.pairService = PairService();

    this.onSubmit = this.onSubmit.bind(this);
    this.onChange = this.onChange.bind(this);
  }

  async onSubmit(event) {
    await this.pairService.submit(this.props.value);
    this.props.valueCallback(DEFAULT_VALUE);
    this.props.refreshCallback();
    event.preventDefault();
  }

  onChange(event) {
    this.props.valueCallback(event.target.value);
  }

  render() {
    return (
        <div className="pure-u-1 pair-form">
          <form class="pure-form" onSubmit={this.onSubmit}>
            <input type="text" class="pure-input-1-2" placeholder="Type here" value={this.props.value} onChange={this.onChange} />
          </form>
        </div>
    );
  }

}

export {DEFAULT_VALUE, PairForm};
