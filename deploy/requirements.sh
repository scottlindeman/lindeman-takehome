#! /usr/bin/env bash

set -e
set -u

function install_ubuntu_basics {
    apt update
    apt install -y make
    apt install -y wget
    apt install -y sudo
    apt install -y curl
}

function install_python {
    apt install -y python3
    apt install -y python3-venv
    python3 -m venv docker_env
    source docker_env/bin/activate
    pip install -r requirements.txt
}

function install_npm {
    curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.37.2/install.sh | bash
    export NVM_DIR="$HOME/.nvm"
    echo $NVM_DIR
    [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
    [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
    nvm install 12.18.4
    echo $(npm -v)
}

function install_sqlite {
    apt install -y sqlite3
    apt install -y libsqlite3-dev
}

install_ubuntu_basics
install_python
install_npm
install_sqlite
