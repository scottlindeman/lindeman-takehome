import pathlib

from flask_sqlalchemy import SQLAlchemy

_TOP_LEVEL = str(pathlib.Path(__file__).parent.parent.absolute())

_DATABASE_LOCATION = f'sqlite:///{_TOP_LEVEL}/aclima.sqlite'

def init_db(app):
    """
    Create the application DB. Should only be called once.

    Params
    ------
    app: FlaskApplication
        The application of a running flask app

    Returns
    -------
    SQLAlchemy Database
    """
    app.config['SQLALCHEMY_DATABASE_URI'] = _DATABASE_LOCATION
    db = SQLAlchemy(app)
    return db
