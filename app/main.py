from flask import Flask, jsonify, request, make_response

from database import init_db

aclima_app = Flask(__name__)
DB = init_db(aclima_app)

import models

# ------------------------------------------------------------------------------

# Quick CORS helpers

# ------------------------------------------------------------------------------

# Ideally you have a better plan on CORS than this
def cors_jsonify(resp_json):
    resp = jsonify(resp_json)
    resp.headers.add('Access-Control-Allow-Origin', '*')
    return resp


@aclima_app.route('/pair', methods=['OPTIONS'])
def cors_check():
    """
    Need to help the UI to access our server on different ports
    """
    resp = make_response()
    resp.headers.add('Access-Control-Allow-Origin', '*')
    resp.headers.add('Access-Control-Allow-Headers', '*')
    resp.headers.add('Access-Control-Allow-Methods', '*')
    return resp

# ------------------------------------------------------------------------------

# Routes for the Application

# ------------------------------------------------------------------------------

@aclima_app.route('/pair', methods=['POST'])
def check_pair():
    """
    Count the pairs of words in a user inputted string

    Request
    -------
    userStr: String
        The input string

    Response
    --------
    [List of EntryResult] Represented as JSON
    """
    req_data = request.json
    if not req_data:
        return "No user input supplied", 422

    user_str = req_data.get('userStr')

    import services.pairs as sp
    pd = sp.PairDetection(db=DB)
    pairs = pd.find_and_save(user_str)

    return cors_jsonify([p.to_json() for p in pairs])

@aclima_app.route('/pair', methods=['GET'])
def list_pairs():
    """
    List the pairs found for each entry

    Response
    --------
    [List of UserEntry] Represented as JSON
    """
    import services.entry as se
    user_entries = se.list()
    return cors_jsonify([ue.to_json() for ue in user_entries])


if __name__ == '__main__':
    aclima_app.run(host='0.0.0.0', port='5000', debug=True)
