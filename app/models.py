from datetime import datetime

from main import DB

class UserEntry(DB.Model):
    __tablename__ = 'user_entry'
    id = DB.Column(DB.Integer, primary_key=True)
    entry = DB.Column(DB.String(128))
    timestamp = DB.Column(DB.DateTime, default=datetime.utcnow)
    results = DB.relationship('EntryResults')

    def to_json(self):
        return {
            'userEntryId': self.id,
            'created': self.timestamp,
            'entry': self.entry,
            'results': [r.to_json() for r in self.results]
        }


class EntryResults(DB.Model):
    __tablename__ = 'entry_results'
    id = DB.Column(DB.Integer, primary_key=True)
    user_entry_id = DB.Column(DB.Integer, DB.ForeignKey('user_entry.id'))
    user_entry = DB.relationship('UserEntry', back_populates='results')
    pair = DB.Column(DB.String(64))
    count = DB.Column(DB.Integer)

    def to_json(self):
        return {
            'entryResultId': self.id,
            'pair': self.pair,
            'count': self.count
        }


# Needed to create the tables
DB.create_all()
