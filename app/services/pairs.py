from collections import defaultdict

class PairDetection:

    WINDOW_SIZE = 2

    def __init__(self, db=None):
        self._db = db
        if not self._db:
            print('No database provided')

    @staticmethod
    def _clean_user_str(user_str):
        """
        Nothing complicated. An improvement would be to strip
        punctuation, but not in the spec!
        """
        return user_str.strip()

    def find_pairs(self, user_str):
        """
        Find the pairs in a string sentence

        Params
        ------
        user_str: String
            The string to analyze

        Returns
        -------
        [Dict of String: Int], String pairs to counts
        """
        cleaned_and_split = PairDetection._clean_user_str(user_str).split(' ')

        hist = defaultdict(int)

        for idx in range(0, len(cleaned_and_split)):
            window = cleaned_and_split[idx:self.WINDOW_SIZE + idx]
            if len(window) < self.WINDOW_SIZE:
                continue

            pair = ' '.join(sorted([x.lower() for x in window]))
            hist[pair] += 1

        return hist

    def save(self, user_entry, pair_histogram):
        """
        Save pairs to the database

        Params
        ------
        user_entry: UserEntry
            The associated UserEntry
        pair_histograms: [Dict of String: Int]
            The string pairs and their counts to save

        Returns
        -------
        [List of EntryResults]
        """
        from models import EntryResults
        all_results = []
        for pair, count in pair_histogram.items():
            results = EntryResults(user_entry_id=user_entry.id, pair=pair, count=count)
            self._db.session.add(results)
            all_results.append(results)

        self._db.session.commit()

        return all_results

    def find_and_save(self, user_str):
        """
        Fine and save pairs in a string in one fell swoop.
        This will also create a UserEntry for the string.

        Params
        ------
        user_str: String
            You know the drill

        Returns
        -------
        [List of EntryResults]
        """
        import services.entry as se
        found = self.find_pairs(user_str)
        user_entry = se.save(self._db, user_str)
        results = self.save(user_entry, found)
        return results
