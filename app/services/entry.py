from models import UserEntry, EntryResults

"""
Module for saving an querying UserEntries and EntryResults
"""

def save(db, user_str):
    """
    Create a new user entry in the db

    Params
    ------
    db: SQLAlchemy Database
        The database
    user_str: String
        The user string

    Returns
    -------
    UserEntry
    """
    entry = UserEntry(entry=user_str)
    db.session.add(entry)
    db.session.commit()
    return entry


def list():
    """
    List ALL of the user submitted entries

    Returns
    -------
    [List of UserEntry]
    """
    return UserEntry.query.all()
