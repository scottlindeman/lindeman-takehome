FROM ubuntu:18.04

SHELL ["/bin/bash", "-c"]

COPY . /opt/aclima
WORKDIR /opt/aclima
RUN ./deploy/requirements.sh

ENV PATH="/opt/aclima/docker_env/bin:/root/.nvm/versions/node/v12.18.4/bin:$PATH"
ENV PYTHONPATH="/opt/aclima"

CMD make run
