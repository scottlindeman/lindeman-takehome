SHELL := /bin/bash

docker-build:
	docker build --tag takehome .

docker-run:
	docker run -p 80:3000 -p 5000:5000 --name takehome-app -d takehome

docker-stop:
	docker stop takehome-app
	docker rm takehome-app

docker-build-for-repo:
	docker build --tag scottlindeman/takehome .

docker-push-latest:
	docker push scottlindeman/takehome

docker-pull-run:
	docker pull scottlindeman/takehome:latest
	docker run -p 80:3000 -p 5000:5000 --name takehome-app -d scottlindeman/takehome:latest

unit-test:
	echo "WARNING: Make sure PYTHONPATH is set to this directory"
	py.test -vvvv tests/app/

run:
	python app/main.py &
	npm run --prefix client start
