import pytest

import app.services.pairs as sp

@pytest.fixture
def pair_detection_no_db():
    return sp.PairDetection()

class TestFindPairs:
    def test_success(self, pair_detection_no_db):
        user_str = "this is a test of finding pairs is this"
        actual = pair_detection_no_db.find_pairs(user_str)
        expected = {
            'is this': 2,
            'a is': 1,
            'a test': 1,
            'of test': 1,
            'finding of': 1,
            'finding pairs': 1,
            'is pairs': 1
        }

        assert actual == expected, actual

    def test_empty(self, pair_detection_no_db):
        user_str = ''
        actual = pair_detection_no_db.find_pairs(user_str)
        expected = {}
        assert actual == expected, actual

    def test_odd(self, pair_detection_no_db):
        user_str = 'one'
        actual = pair_detection_no_db.find_pairs(user_str)
        expected = {}
        assert actual == expected, actual

    def test_same_words(self, pair_detection_no_db):
        user_str = 'create create create create'
        actual = pair_detection_no_db.find_pairs(user_str)
        expected = {'create create': 3}
        assert actual == expected, actual

    def test_punctuation(self, pair_detection_no_db):
        user_str = 'this, and this'
        actual = pair_detection_no_db.find_pairs(user_str)
        expected = {
            'and this': 1,
            'and this,': 1
        }
        assert actual == expected, actual
